# MessageBar

#### 基本介绍

基于MessageTools开发的消息显示控件，用作窗体底部的消息栏。有可选的依据消息等级的图标显示与背景色切换、淡入淡出动效等功能。依赖于MessageTools.dll。


#### 开发环境

    1.IDE：Visual Studio 2017

    2.Platform：.Net Framework 4.6.1

    3.Type：类库->用户控件（.dll）


#### 软件架构

![软件架构](https://images.gitee.com/uploads/images/2021/0118/114051_531e0b28_8543418.png "MessageBar.PNG")


#### 安装教程

    1.将此类库放置在需要引用的工程目录下。

    2.在解决方案资源管理器中找到“引用”，单击右键，浏览，选择此.dll。

    3.在工具箱中找到合适的分类选项卡，单击右键，点击“选择项”。

    4.在.Net Framwork选项卡下点击浏览，选择MessageBar.dll，导入成功。

（PS：此控件需要MessageTools.dll作为前置依赖）


#### 使用说明

像普通的WinForm窗体控件一般使用即可。在属性栏中，新增了“动画”、“消息属性”与“消息显示”等属性设置，重写了Font、ForeColor与BackColor等属性。具体属性介绍详见Visual Studio中的属性窗口。

如果想个性化定制或深度修改此项目，可以下载[-
[MessageTools](https://gitee.com/slash-bean/msgtools) -]以修改基类。

