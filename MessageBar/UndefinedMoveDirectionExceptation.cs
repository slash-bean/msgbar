﻿using System;

namespace MessageBar
{
    partial class UndefinedMoveDirectionExceptation : Exception
    {
        public UndefinedMoveDirectionExceptation() { }
        public UndefinedMoveDirectionExceptation(string Message) : base(Message) { }
    }
}
